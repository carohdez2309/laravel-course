@extends('layouts.auth')

@section('content')
<div class="row auth-box">
    <div class="col-md-6 col-sm-12 col-xs-12 auth-box-image text-center">
        <img src="/images/banners/logo.png" alt="SX Plus" width="80%">
        <div class="cubes d-none d-sm-block d-md-block"></div>
    </div>
    <div class="col-md-6 col-sm-12 col-xs-12 auth-box-body">
        <div class="card-body">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="row mb-5">
                    <div class="col-md-12 text-center">
                        ¿Aún no tienes cuenta? <strong><a class="text-sx" href="{{ route('register') }}">{{ __('Registrarse') }}</a></strong>
                    </div>
                </div>

                <div class="row col-md-12 mb-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope text-custom"></i></span>
                        </div>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Dirección de correo" required autofocus>
                        
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row col-md-12 mb-3">   
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-lock text-custom"></i></span>
                        </div>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Contraseña" required>
                        
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Recordarme') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row mb-5">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-block btn-orange">
                            {{ __('Inciar sesión') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('¿Olvidaste tu contraseña?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
