<!-- Home -->
<li class="nav-item"><a href="{!! route('home') !!}" class="nav-link {{ Request::is('home')  ? 'active' : '' }}"><i class="nav-icon fa-solid fa-house"></i><p class="levelOne">Dashboard</p></a></li>

<!-- Books -->
<li class="nav-item"><a href="#" class="nav-link {{ Request::is('books*') ? 'active' : '' }}"><i class="nav-icon fa-solid fa-book"></i></i><p class="levelOne">Libros</p></a></li>

<!-- Administración -->
<li class="nav-item has-treeview {{ Request::is('bitacoras*') || Request::is('activitiestypes*') ? 'menu-open' : '' }}">
    <a href="#" class="nav-link {{ Request::is('bitacoras*') || Request::is('activitiestypes*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-video"></i><p class="levelOne">Visuales<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item"><a href="#" class="nav-link {{ Request::is('movies*') ? 'active' : '' }}"><i class="nav-icon fa-solid fa-film icono"></i><b class="levelTwo">Películas</b></a></li>
        <li class="nav-item"><a href="#" class="nav-link {{ Request::is('series*') ? 'active' : '' }}"><i class="nav-icon fas fa-tv icono"></i><b class="levelTwo">Series</b></a></li>
    </ul>
</li>

<!-- Authors -->
<li class="nav-item"><a href="#" class="nav-link {{ Request::is('authors*') ? 'active' : '' }}"><i class="nav-icon fas fa-users"></i><p class="levelOne">Autores</p></a></li>

<div class="separator"></div>

<!-- Administración -->
<li class="nav-item has-treeview {{ Request::is('bitacoras*') || Request::is('activitiestypes*') ? 'menu-open' : '' }}">
    <a href="#" class="nav-link {{ Request::is('bitacoras*') || Request::is('activitiestypes*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-user-cog"></i><p class="levelOne">Administración <i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item"><a href="#" class="nav-link {{ Request::is('bitacoras*') ? 'active' : '' }}"><i class="fas fa-cog nav-icon icono"></i><b class="levelTwo">Géneros</b></a></li>
        <li class="nav-item"><a href="#" class="nav-link {{ Request::is('myTenancies*') ? 'active' : '' }}"><i class="nav-icon fas fa-cog icono"></i><b class="levelTwo">Otros</b></a></li>
    </ul>
</li>


