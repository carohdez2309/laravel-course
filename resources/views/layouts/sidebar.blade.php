<aside class="main-sidebar sidebar-light-primary elevation-4" id="sx-sidebar">
    <!--BANNER-->
    <a href="/home" class="brand-link">
        Imagen
    </a>
    
    <div class="sidebar">
        <!--USER INFO-->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <div class="img-circle elevation-2" style="background-image: url('https://cache.conversso.com/stg-a/sxbp/1/2021/09/09/sxbp-613a8e557725e.png')"></div>
                <a href="#"><i class="fas fa-cog setting-icon"></i></a>
            </div>
            <div class="info">
                <a href="#" class="d-block font-weight-bold"> 
                    {{ Auth::user()->name }}
            </div>
        </div>
        
        <!--MENU TREE-->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @include('layouts.menu')
            </ul>
        </nav>
    </div>
</aside>