<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SX PLUS</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="/dist/css/adminlte.css">
    <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="/plugins/toastr/toastr.css">
    <link rel="stylesheet" href="/plugins/loadMask/jquery.loadmask.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css">
    
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300&display=swap" rel="stylesheet">
    <link href="/plugins/fontawesome-free/css/all.css" rel="stylesheet">

    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/images/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/icon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/icon/favicon-16x16.png">
    <link rel="icon" href="/images/icon/favicon-16x16.png" type="image/x-icon"/>

    <link rel="stylesheet" href="/css/sx.css">
    @yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <div id="wrapper">
        <!--NAVBAR-->
        <nav class="main-header navbar navbar-expand navbar-custom">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button">
                        <i class="fas fa-bars text-white"></i>
                    </a>
                </li>
            </ul>
            
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" title="Cumpleaños" href="#" role="button">
                        <i class="fas fa-birthday-cake text-white"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" title="Notificaciones" href="#" role="button">
                        <i class="fas fa-bell text-white"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" title="Solicitudes" href="#" role="button">
                        <i class="fas fa-exchange text-white"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="dropdown" title="Soporte" href="#" role="button">
                        <i class="fas fa-info-circle text-white"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="logout" data-toggle="dropdown" title="Cerrar sesión" role="button">
                        <i class="fas fa-sign-out text-white"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>

        @include('layouts.sidebar')

        <div class="content-header"></div>

        <div class="content-wrapper">
            <div class="content">
                <div class="clearfix"></div>

                <div class="box box-primary">
                    <div class="box-body">
                        @yield('content')
                    </div>
                </div>

            </div>
        </div>

        <footer class="main-footer">
            <strong>Copyright &copy; {{ date('Y') }} <a href="#">SaleXops Plus</a>. </strong>
            <div class="float-right d-none d-sm-inline-block">
                <a href="#" target="_blank">Términos y condiciones.</a>
                <b>Version</b> 2.0
            </div>
        </footer>
    </div>

    <script src="/plugins/jquery/jquery.min.js"></script>
    <script src="/dist/js/adminlte.js"></script>
    <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <script src="/plugins/moment/moment.min.js"></script>
    <script src="/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="/plugins/select2/js/select2.min.js"></script>
    <script src="/plugins/select2/js/i18n/es.js"></script>
    <script src="/plugins/toastr/toastr.min.js"></script>
    <script src="/plugins/inputmask/jquery.inputmask.js"></script>
    <script src="/plugins/loadMask/jquery.loadmask.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>

    @yield('scripts')
    @yield('scripts2')

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd",
                language: "es",
                autoclose: true,
                orientation: 'bottom auto'
            });

            $(".date").inputmask('9999-99-99');
        });

        maskform = function(){
            $("#maskfullscreen").css("z-index", 99999).mask("Guardando información, espere...");
        };
    
        unmaskform = function(){
            $("#maskfullscreen").css("z-index", -1).unmask();
        };

        $("#logout").on("click", function() {
            Swal.fire({
                title: 'Cerrar sesión',
                html: '¿Desea cerrar sesión?',
                type: "warning",
                confirmButtonColor: "#0074C5",
                cancelButtonColor: "#E0E0E0",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Cancelar",
                showCancelButton: true,
            }).then(function(data){
                if (data.value) {
                    $('#logout-form').submit();
                }
            });
        });
    </script>
</body>
</html>
