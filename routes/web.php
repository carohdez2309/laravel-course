<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/configcache', function () {
    $e1 = Artisan::call('config:cache');
    $e2 = Artisan::call('config:clear');
    $e3 = Artisan::call('cache:clear');
    $e4 = Artisan::call('migrate', array('--force' => true));
    dd('You have cleared all cache application', $e1, $e2, $e3, $e4);
});

Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return redirect('/login');
    });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});


