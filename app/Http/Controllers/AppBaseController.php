<?php

namespace App\Http\Controllers;

use Response;
use InfyOm\Generator\Utils\ResponseUtil;
use League\Fractal\Serializer\ArraySerializer;


class AppBaseController extends Controller
{
    protected $errors = false;

    protected function jsonResponse($resource)
    {
        $response = [
            'message'   => $resource->message,
            'code'      => $resource->code,
            'isError'   => $resource->isError,
            'errors'    => $resource->errors,
            'data'      => ''
        ];

        if(isset($resource->contact)) $response['contact'] = $resource->contact;

        if(isset($resource->resource)) {
            $response['data'] = $resource->resource;
        } else {
            $response['data'] = $resource->modelo;
        }

        return response()->json($response, ($resource->code != 204)  ? $resource->code : 200);
    }

    public function setErrors($errors) {
        $this->errors = $errors;
    }

    public function getErrors() {
        return $this->errors;
    }

    protected function serializeArray()
    {
        return new ArraySerializer();
    }

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    public function setLog($controller, $method, $message, $trace) {
        //
    }

}
