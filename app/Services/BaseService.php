<?php

namespace App\Services;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;

class BaseService extends AppBaseController
{
    public function dataTable(Request $request){ }

    public function add(Request $request){ }

    public function update($id, Request $request){ }

    public function find($id){ }

    public function delete($id){ }

}